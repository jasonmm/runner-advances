-- name: retrosheet-db-check
-- Used to ensure we are connected to a Retrosheet database.
SELECT GAME_ID, EVENT_TX FROM events LIMIT 1

-- name: on-base-events
-- Get a all events where `player_id` is on base. Events are limited by year
-- to prevent scanning the entire table.
SELECT GAME_ID, YearId, EVENT_TX, BASE1_RUN_ID, BASE2_RUN_ID, BASE3_RUN_ID
FROM events
WHERE YearId BETWEEN :start_year AND :end_year
  AND ((BASE1_RUN_ID=:player_id) OR (BASE2_RUN_ID=:player_id) OR (BASE3_RUN_ID=:player_id))

-- name: bdb-db-check
-- Used to ensure we are connected to a Baseball DataBank database.
SELECT yearID, AB FROM batting LIMIT 1

-- name: player-years
-- Gets the first and last years played by a player and their ID in the
-- Retrosheet database.
SELECT p.retroID, min(b.yearID) AS firstYear, max(b.yearID) AS lastYear
FROM batting b INNER JOIN people p ON (b.playerID=p.playerID)
WHERE b.playerID=:player_id
GROUP BY b.playerID
