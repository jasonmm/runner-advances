# Description

This program takes Baseball DataBank player IDs as command-line arguments and counts the number of times each player advanced each number of bases.

# Requirements

* A Baseball Databank database
* A [Retrosheet database](https://gitlab.com/jasonmm/how-to/blob/master/create/retrosheet-database.md)

# Usage

```bash
$ lein run almonzo01 belljo01
({:playerId "almonzo01", :base-advances {1 21, 2 5, 3 3, 0 4}}
 {:playerId "belljo01", :base-advances {1 48, 0 1, 2 11, 3 3}})
```

The above output shows that when "almonzo01" was on base there were 21 plays where he advanced one base and 3 plays where he advanced 3 bases in his career.
