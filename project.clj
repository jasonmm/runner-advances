(defproject runner-advances "0.1.0-SNAPSHOT"
  :description "Reads `retrosheet` data to gather running advancing data."
  :dependencies [[org.clojure/clojure "1.8.0"]
                 [jasonmm/retrosheet-event "0.1.0"]
                 [mysql/mysql-connector-java "5.1.6"]
                 [environ "1.1.0"]
                 [yesql "0.5.3"]
                 [instaparse "1.4.7"]]
  :plugins [[lein-environ "1.0.2"]]
  :main ^:skip-aot runner-advances.core
  :target-path "target/%s"
  :profiles {:dev       [:profile-env]
             :uberjar   {:aot :all}})
