(ns runner-advances.core
  (:gen-class)
  (:require [retrosheet-event.core :as rsevent]
            [environ.core :refer [env]]
            [instaparse.core :as insta]
            [clojure.java.jdbc :as jdbc]
            [clojure.java.io :as io]
            [runner-advances.sql :as sql]))

;;; Database connection to Retrosheet data.
(def retro-db {:classname   "com.mysql.jdbc.Driver"
               :subprotocol "mysql"
               :subname     (str "//" (env :retro-db-host) ":3306/" (env :retro-db-name))
               :user        (env :retro-db-user)
               :password    (env :retro-db-pass)})

;;; Database connection to Baseball DataBank data.
(def bdb-db {:classname   "com.mysql.jdbc.Driver"
             :subprotocol "mysql"
             :subname     (str "//" (env :bdb-db-host) ":3306/" (env :bdb-db-name))
             :user        (env :bdb-db-user)
             :password    (env :bdb-db-pass)})

(insta/set-default-output-format! :hiccup)

(defn starting-base
  "Returns the number of the base `player-id` started from."
  [player-id result-record]
  (cond
    (= player-id (:base1_run_id result-record)) 1
    (= player-id (:base2_run_id result-record)) 2
    (= player-id (:base3_run_id result-record)) 3))

(defn transform-successful-advance
  [start-base dash end-base & advance-info]
  [:successful-advance start-base end-base])

(defn transform-unsuccessful-advance
  [start-base x end-base & out-info]
  [:unsuccessful-advance start-base end-base])

(defn transform-advances-key
  [& advance-items]
  [:advances (filter vector? advance-items)])

(defn transform-advances
  "Transforms base running advances into something easier to use.
  `parsed-event` should be in `:hiccup` format (see Instaparse docs)"
  [parsed-event]
  (insta/transform {:BAG                  (fn [x] (Integer/parseInt x))
                    :HOME                 (fn [x] 4)
                    :successful-advance   transform-successful-advance
                    :unsuccessful-advance transform-unsuccessful-advance
                    :advance              (fn [advance-vector] advance-vector)
                    :advances             transform-advances-key}
                   parsed-event))

(defn get-advances
  [parsed-event]
  (second (first (filter #(and (vector? %) (= :advances (first %))) parsed-event))))

(defn get-player-advance-data
  "Returns a vector describing the advance made by the player on `base`."
  [base advances]
  (filter #(= base (second %)) advances))

(defn num-bases-advanced
  [advances]
  (let [player-advance (get-player-advance-data)]))

(defn bases-advanced
  "Returns the number of bases the player advanced in the play. 0 means the
  player did not advance, most likely due to an out."
  [player-id event-record]
  (let [player-base    (starting-base player-id event-record)
        advances       (-> event-record
                           :event_tx
                           rsevent/parse
                           transform-advances
                           get-advances)
        player-advance (get-player-advance-data player-base advances)]
    (if (or (nil? player-advance) (empty? player-advance))
      -1
      (let [player-advance (first player-advance)]
        (if (= :unsuccessful-advance (first player-advance))
          0
          (if (> 3 (count player-advance))
            (println (count player-advance))
            (- (nth player-advance 2) player-base)))))))

(defn bases-advanced-counts
  "Returns a map of the number of times the player advanced a number of bases.
  Generally, `{<# bases advanced> <count>}`."
  [player-id start-year end-year]
  (let [event-records (sql/on-base-events {:player_id  player-id
                                           :start_year start-year
                                           :end_year   end-year}
                                          {:connection retro-db})]
    (->> event-records
         (pmap (fn [event-record] (bases-advanced player-id event-record)))
         (filter #(or (zero? %) (pos? %))))))

(defn process-player
  "Process the given player."
  [bdb-player-id]
  (->> (sql/player-years {:player_id bdb-player-id} {:connection bdb-db})
       first
       (into [])
       (map second)
       (apply bases-advanced-counts)))

(defn valid-databases?
  "Simple checks to determine if the database connections are to databases with
  the right data."
  []
  (and
   (= 1 (count (sql/bdb-db-check {} {:connection bdb-db})))
   (= 1 (count (sql/retrosheet-db-check {} {:connection retro-db})))))

(defn -main
  [& args]
  (if (valid-databases?)
    (->> args
         (map (fn [player-id] {:playerId player-id :base-advances ((comp frequencies process-player) player-id)}))
         clojure.pprint/pprint)))

(comment
  (process-player "sislege01")
  (time (frequencies (process-player "mantlmi01")))
  (time (frequencies (process-player "almonzo01")))
  (time (frequencies (process-player "sislege01")))

  (time (frequencies (bases-advanced-counts "thomf001" 1990 1991)))
  (def p (rsevent/parse "S7/L56D.3-H;2XH(72);1-2"))
  (def t (filter #(and (vector? %) (= :advances (first %))) (transform-advances p)))
  (def a (get-advances t))
  (clojure.pprint/pprint a)
  (clojure.pprint/pprint t)
  (get-advances [:event
                 [:description [:fielder "3"] [:fielder "1"]]
                 [:advances
                  "."
                  [:advance [:successful-advance [:BAG "1"] "-" [:BAG "2"]]]]])
                                    ;
)
